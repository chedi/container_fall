#!/bin/bash

set -e

export DATA_CONTAINER="chedi/psql-9.3:f20"
export CODE_CONTAINER="chedi/django-1.6:f20"

export APPLICATION_NAME="epsilon"
export APPLICATION_REPO="git@bitbucket.org:chedi/epsilon_comming_soon.git"

export APPLICATION_DATA_PATH="/var/data/psql"
export APPLICATION_CODE_PATH="/var/data/application"

export DATA_CID="data_container.cid"
export CODE_CID="code_container.cid"

docker pull $DATA_CONTAINER
docker pull $CODE_CONTAINER

mkdir -p "$APPLICATION_DATA_PATH"
mkdir -p "$APPLICATION_CODE_PATH"

docker run -t -d -P -v "$APPLICATION_DATA_PATH":/var/lib/pgsql "$DATA_CONTAINER"

docker run -t -d -P -v "$APPLICATION_CODE_PATH":"/var/www/project/$APPLICATION_NAME" \
-e APPLICATION_NAME="$APPLICATION_NAME" -e APPLICATION_REPO="$APPLICATION_REPO"      \
-e SSH_KEY="$(<id_rsa)" "$CODE_CONTAINER"                      
